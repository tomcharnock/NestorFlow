import tensorflow as tf
import numpy as np
from nestorflow.prior import prior_sample
from nestorflow.slice import slice_sample
from nestorflow.likelihood import wrap_likelihood

def evidence(dead_L):
    n_live = tf.cast(tf.shape(dead_L)[1], dtype=tf.float32)
    i = tf.range(tf.cast(tf.size(dead_L), dtype=tf.float32))
    logw = -i/n_live + tf.math.log(tf.math.sinh(1/n_live)) + tf.sort(tf.reshape(dead_L, (-1,)))
    logZ = tf.math.reduce_logsumexp(logw)
    return logZ


def samples(dead_points, dead_L):
    n_live = tf.cast(tf.shape(dead_L)[1], dtype=tf.float32)
    i = tf.range(tf.cast(tf.size(dead_L), dtype=tf.float32))
    j = tf.argsort(tf.reshape(dead_L, (-1,)))
    posterior = tf.reshape(dead_points,[tf.shape(dead_points)[0]*tf.shape(dead_points)[1],tf.shape(dead_points)[2]])
    logw = -i/n_live + tf.math.log(tf.math.sinh(1/n_live)) + tf.sort(tf.reshape(dead_L, (-1,)))
    logw -=  tf.math.reduce_logsumexp(logw) 
    return posterior, logw
    

def nested_sample(input_likelihood, n_live, n_dims, n_repeats):
    likelihood = wrap_likelihood(input_likelihood)

    def cond(live_points, dead_points, L, dead_L, w, i):
        live_logZ = tf.reduce_max(L - tf.cast(tf.shape(dead_L)[0], dtype=tf.float32))
        logZ = evidence(dead_L)
        return tf.greater(live_logZ, logZ + tf.cast(tf.log(1e-2), dtype=tf.float32))

    def body(live_points, dead_points, L, dead_L, w, i):
        dead_points = tf.concat([dead_points, tf.expand_dims(live_points, 0)], axis=0) 
        dead_L = tf.concat([dead_L, tf.expand_dims(L, 0)], axis=0) 
        live_points, L, w = slice_sample(likelihood, live_points, L, n_repeats, w)
        return [live_points, dead_points, L, dead_L, w, tf.add(i, 1)]

    i = tf.constant(0)
    live_points, L = prior_sample(likelihood, n_live, n_dims)
    w = tf.ones(n_live)

    dead_points = tf.Variable(np.zeros((0, n_live, n_dims),dtype=np.float32))
    dead_L = tf.Variable(np.zeros((0, n_live),dtype=np.float32))

    shape_invariants = [live_points.get_shape(),
                        tf.TensorShape([None, n_live, n_dims]),
                        L.get_shape(),
                        tf.TensorShape([None, n_live]),
                        w.get_shape(),
                        i.get_shape()] 
    loop = tf.while_loop(cond, body, [live_points, dead_points, L, dead_L, w, i], shape_invariants=shape_invariants) 
    return loop[1], loop[3]
