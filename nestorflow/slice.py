import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions

def right_bound(likelihood, live_points, min_L, n_hat, u):
    """ Returns the while loop which generates r_bound, and r_L"""
    def cond(r_bound, r_L):
        return tf.reduce_any(tf.math.greater_equal(r_L, min_L))

    def body(r_bound, r_L):
        r_bound_next = tf.add(r_bound, tf.where(tf.math.greater_equal(r_L, min_L), n_hat, tf.zeros_like(n_hat)))
        return [r_bound_next, likelihood(r_bound_next)]

    r_bound = tf.add(live_points, tf.multiply(n_hat, u))
    r_L = likelihood(r_bound)
    loop = tf.while_loop(cond, body, [r_bound, r_L])
    return loop[0], loop[1]


def left_bound(likelihood, live_points, min_L, n_hat, u):
    """ Returns the while loop which generates l_bound, and l_L"""
    def cond(l_bound, l_L):
        return tf.reduce_any(tf.math.greater_equal(l_L, min_L))

    def body(l_bound, l_L):
        l_bound_next = tf.subtract(l_bound, tf.where(tf.math.greater_equal(l_L, min_L), n_hat, tf.zeros_like(n_hat)))
        return [l_bound_next, likelihood(l_bound_next)]

    l_bound = tf.add(live_points, tf.multiply(n_hat, tf.subtract(u, 1)))
    l_L = likelihood(l_bound)
    loop = tf.while_loop(cond, body, [l_bound, l_L])
    return loop[0], loop[1]


def single_slice(likelihood, live_points, min_L, n_hat, w):
    """ Performs a single slice sampling step on all the points"""
    uniform = tfd.Uniform()
    n_live = live_points.get_shape().as_list()[0]
    u = uniform.sample((n_live, 1))

    def cond(live_points_next, L, r_b, r_L, l_b, l_L):
        return tf.reduce_any(tf.less(L, min_L))

    def body(live_points_next, L, r_b, r_L, l_b, l_L):
        out = tf.less(L, min_L)
        rhs = tf.math.greater_equal(tf.einsum("ij,ij->i", n_hat, tf.subtract(live_points_next, live_points)), tf.zeros(n_live))

        move_right = tf.logical_and(out, rhs)
        r_bound_next = tf.where(move_right, live_points_next, r_bound)

        move_left = tf.logical_and(out, tf.logical_not(rhs))
        l_bound_next = tf.where(move_left, live_points_next, l_bound)

        u3 = uniform.sample((n_live, 1))
        live_points_next = tf.where(out, tf.add(tf.multiply(tf.subtract(r_bound_next, l_bound_next), u3), l_bound_next), live_points_next)
        return [live_points_next, likelihood(live_points_next), r_bound_next, likelihood(r_bound_next), l_bound_next, likelihood(l_bound_next)]

    l_bound, l_L = left_bound(likelihood, live_points, min_L, n_hat, u) 
    r_bound, r_L = right_bound(likelihood, live_points, min_L, n_hat, u)

    w = tf.einsum("ij,ij->i", n_hat, tf.subtract(r_bound, l_bound))

    u2 = uniform.sample((n_live, 1))
    live_points_next = tf.add(tf.multiply(tf.subtract(r_bound, l_bound), u2), l_bound)
    L = likelihood(live_points_next)

    loop = tf.while_loop(cond, body, [live_points_next, L, r_bound, r_L, l_bound, l_L])
    return loop[0], loop[1], w


def slice_sample(likelihood, live_points, L, n_repeats, w):
    n_live, n_dims = live_points.get_shape().as_list()
    direction_generator = tfp.distributions.Normal(loc=0., scale=1.)

    def cond(live_points, L, w, i):
        return tf.less(i, n_repeats)

    def body(live_points, L, w, i):
        directions = direction_generator.sample((n_live, n_dims))
        n_hat = tf.divide(directions, tf.norm(directions, axis=1, keepdims=True))
        #n_hat = tf.multiply(tf.expand_dims(w,1), n_hat)
        live_points, L, w = single_slice(likelihood, live_points, min_L, n_hat, w)
        return [live_points, L, w, tf.add(i, 1)]

    i = tf.constant(0)
    min_L = tf.identity(L)
    loop = tf.while_loop(cond, body, [live_points, L, w, i]) 
    return loop[0], loop[1], w
