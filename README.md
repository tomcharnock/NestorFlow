# NestorFlow (anag)

## Basic nested sampling algorithm

Nested sampling is method for generating posterior samples from a likelihood whilst simultaneously computing Bayesian evidences. It can be thought of as alternative MCMC technique.

![](NestedSamplingBasic.embed.svg)

0. Draw N 'live points' from the prior, and evaluate their likelihoods
1. Delete the lowest likelihood live point, and replace it with a new point drawn from the prior, but at higher likelihood
2. Repeat Step 1. until some stopping criteria is met.

Under this procedure one has an evolving set of live points that gradually contracts around the peak of the posterior. Advantages:

* Since one is sampling from the prior, one can compute Bayesian evidences, Kullback Liebler divergences and partition functions from the likelihood.
* The evolving set of live points adapt slowly to degeneracies and multimodalities, allowing it to 'learn' any difficulties as it goes along.

### Slice sampling

We are going to use hit-and-run slice sampling to find our new points:

![](NestedSampling.embed.svg)


## Some code

We load our modules including tensorflow probability for our likelihood.
```python
import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
from nestorflow.nested import nested_sample
import matplotlib.pyplot as plt
```

We define some global variables -- these will eventually be inputs to the sampler:
 * `n_dims`: Number of parameters
 * `n_live`: Number of live points
 * `n_repeats`: Number of slice sampling sets
 * `n_compress`: Number of times to run a nested sampling compression
 * `likelihood`: the likelihood to sample over $L : [0,1]^D -> (-\infty,\infty)$

```python
n_dims = 5
n_live = 100
n_repeats = n_dims*2
n_compress = n_dims*5
```

Our test likelihood is a diagonal multivariate Gaussian with means $\mu=0.5$ and diagonal covariance $\Sigma = \textrm{Diag}(0.1)$.
```python
def likelihood(x):
    dist = tfp.distributions.MultivariateNormalDiag(loc=(np.ones(n_dims)*0.5).astype(np.float32), scale_diag=(np.ones(n_dims)*0.1).astype(np.float32))
    return dist.log_prob(x)
```

The nested sampling operation is then defined using
```python
points, L = nested_sample(likelihood, n_live, n_dims, n_repeats, n_compress)
```

We can then run the nested sampler in tensorflow

```python
sess = tf.Session()
sess.run(tf.global_variables_initializer())
likelihoods, samples = sess.run([L, points])
```

The points move up the likelihood as we want - yippee!

![](animation.gif)
