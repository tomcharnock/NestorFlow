#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(name='nestorflow',
      version='0.0.0',
      description='NestorFlow (anag): Nested Sampling in TensorFlow',
      author='Will Handley & Tom Charnock',
      author_email='wh260@cam.ac.uk',
      url='https://github.com/tomcharnock/NestorFlow',
      packages=find_packages(),
      install_requires=['numpy', 'tensorflow'],
      classifiers=[
                   'Development Status :: 3 - Alpha'
                   'Intended Audience :: Developers',
                   'Intended Audience :: Science/Research',
                   'Natural Language :: English',
                   'Programming Language :: Python :: 2.7',
                   'Programming Language :: Python :: 3.4',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6',
                   'Topic :: Scientific/Engineering',
                   'Topic :: Scientific/Engineering :: Astronomy',
                   'Topic :: Scientific/Engineering :: Physics',
                   'Topic :: Scientific/Engineering :: Mathematics',
                   'Topic :: Scientific/Engineering :: Information Analysis',
      ],
      )
